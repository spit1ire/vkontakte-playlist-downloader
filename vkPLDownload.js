let container = document.querySelectorAll('.audio_row__inner'); // main class of audio HTML element
let m3u = '#EXTM3U\n';

'#EXTINF:-1, Artist - Name\n'

for (const iterator of container) {
  const track = iterator.children[0].innerText.match(/(.*)\n(.*)/);
  const artist = track[1];
  const name = track[2];

  m3u = m3u + `#EXTINF:${iterator.children[1].innerText}, ${artist} - ${name}\n`

}

// Just download function
function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

console.log(m3u);

download('vkPlaylist.m3u', m3u)
